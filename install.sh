#!/bin/bash

configs=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
echo "dotfiles dir: $configs"

git -C $configs submodule init
git -C $configs submodule update

ln -s $configs/zshrc ~/.zshrc
ln -s $configs/vimrc ~/.vimrc
ln -s $configs/tmux.conf ~/.tmux.conf

ln -s $configs/gitignore_global ~/.gitignore_global
git config --global core.excludesfile ~/.gitignore_global

mkdir -p ~/.vim/bundle
ln -s $configs/neobundle.vim ~/.vim/bundle/neobundle.vim

