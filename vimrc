" Note: Skip initialization for vim-tiny or vim-small.
if !1 | finish | endif

if has('vim_starting')
  set nocompatible               " Be iMproved

  " Required:
  set runtimepath+=~/.vim/bundle/neobundle.vim/
endif

" Required:
call neobundle#begin(expand('~/.vim/bundle/'))

" Let NeoBundle manage NeoBundle
" Required:
NeoBundleFetch 'Shougo/neobundle.vim'

" My Bundles here:
" Refer to |:NeoBundle-examples|.
" Note: You don't set neobundle setting in .gvimrc!

" syntaxes
NeoBundle 'wting/rust.vim'
NeoBundle 'ealasu/vim-coffee-script'
NeoBundle 'fatih/vim-go'
NeoBundle 'cespare/vim-toml'
NeoBundle 'digitaltoad/vim-jade'

" other plugins
"NeoBundle 'tpope/vim-commentary'
NeoBundle 'tpope/vim-fugitive'
"NeoBundle 'kana/vim-fakeclip'
NeoBundle 'tpope/vim-sensible'
NeoBundle 'tpope/vim-sleuth'
NeoBundle 'tpope/vim-dispatch'
NeoBundle 'camelcasemotion'
"NeoBundle 'ebfe/vim-racer'
NeoBundle 'scrooloose/nerdcommenter'
NeoBundle 'scrooloose/syntastic'
NeoBundle 'nvie/vim-flake8'
NeoBundle 'majutsushi/tagbar'
NeoBundle 'Chiel92/vim-autoformat'
"NeoBundle 'mmai/vim-zenmode'

" auto-completion
NeoBundle 'davidhalter/jedi-vim'
"NeoBundle 'phildawes/racer'

call neobundle#end()

" Required:
filetype plugin indent on

" If there are uninstalled bundles found on startup,
" this will conveniently prompt you to install them.
NeoBundleCheck

syntax on

set sw=4
set expandtab
set softtabstop=2
set tabstop=2
set shiftwidth=2
set autoindent
set cindent
set mouse=a
"set clipboard=unnamed
set clipboard+=unnamedplus

function! TrimWhiteSpace()
  %s/\s\+$//e
endfunction
autocmd BufWritePre     *.coffee :call TrimWhiteSpace()

" Jump to the last position when reopening a file
if has("autocmd")
  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif

nmap <F8> :TagbarToggle<CR>

autocmd FileType rust let b:dispatch = 'cargo build'
nnoremap <F9> :Dispatch<CR>

set hidden
let g:racer_cmd = "/home/eman/workspace/github/racer/target/release/racer"

noremap <F3> :Autoformat<CR><CR>

if has('python')
py << EOF
import os.path
import sys
import vim
if 'VIRTUAL_ENV' in os.environ:
    project_base_dir = os.environ['VIRTUAL_ENV']
    sys.path.insert(0, project_base_dir)
    activate_this = os.path.join(project_base_dir, 'bin/activate_this.py')
    execfile(activate_this, dict(__file__=activate_this))
EOF
endif
