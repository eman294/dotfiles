#!/usr/bin/zsh

DIR=$1
FILE=$2

curl -v -o /dev/null --user "$SMARTFILE_USER:$SMARTFILE_PASS" -F "file=@${FILE}" "https://app.smartfile.com/api/2/path/data/$DIR"

