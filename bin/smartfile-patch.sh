#!/bin/bash

FILE=$1
PATCH_FILE=$2

curl -v --user "$SMARTFILE_USER:$SMARTFILE_PASS" -F "delta=@${PATCH_FILE}" "https://app.smartfile.com/api/2/path/sync/patch/$FILE"

