#!/bin/bash

FILE=$1
shift

curl -v --user "$SMARTFILE_USER:$SMARTFILE_PASS" $@ "https://app.smartfile.com/api/2/path/data/$FILE"

