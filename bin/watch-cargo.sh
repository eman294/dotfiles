#!/bin/zsh
set -e

export RUST_BACKTRACE=1
pid=''

run () {

  if [ $pid ] && [ -e "/proc/$pid" ]; then
    echo "killing pid $pid"
    kill $pid
    #wait $pid
  fi

  echo 'building...'
  cargo build || true

  echo 'running...'
  #cargo run &
  ./target/debug/sm-proxy &
  pid=$!
  echo "pid: $pid"
}

run

while true
do 
  res=$(inotifywait --format '%w%f' -q -r -e modify,close_write,move,create,delete .)
  if [[ $res == *.rs ]]; then
    clear
    run
  fi
done
