#!/bin/bash

FILE=$1
SIGNATURE_FILE=$2

curl -v --user "$SMARTFILE_USER:$SMARTFILE_PASS" -F "signature=@${SIGNATURE_FILE}" "https://app.smartfile.com/api/2/path/sync/delta/$FILE"

