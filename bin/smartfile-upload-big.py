#!/usr/bin/env python
# depends on python-pycurl

import os
import pycurl


up_dir = os.getenv('UP_DIR', 'public')
up_user = os.getenv('SMARTFILE_USER')
if not up_user:
    raise Exception('user not set')
up_pass = os.getenv('SMARTFILE_PASS')
if not up_pass:
    raise Exception('pass not set')


#@retry(stop_max_attempt_number=4)
def upload(src, dest):
    def progress(down_t, down, up_t, up):
        print('{} of {}, {} of {}'.format(down, down_t, up, up_t))

    c = pycurl.Curl()
    c.setopt(c.VERBOSE, 1)
    c.setopt(c.PROGRESSFUNCTION, progress)
    c.setopt(c.POST, 1)
    c.setopt(c.URL, 'https://app.smartfile.com/api/2/path/data/'+dest)
    c.setopt(c.USERNAME, up_user)
    c.setopt(c.PASSWORD, up_pass)
    c.setopt(c.HTTPPOST, [
        ('file', (
            c.FORM_FILE, src,
            c.FORM_CONTENTTYPE, 'application/octet-stream'))
        ])
    c.perform()
    r = c.getinfo(c.HTTP_CODE)
    c.close()
    if r > 399:
        raise Exception('upload failed')


def main(file, dest):
    size = os.stat(file).st_size
    start = 0
    i = 0
    piece_len = 1024*1024*1024
    left = size
    while start < size:
        print(i)
        small_file = os.path.join('/tmp', os.path.basename(file) + '.' + str(i))
        with open(file, 'rb') as r:
            r.seek(start)
            with open(small_file, 'wb') as w:
                written = 0
                while start < piece_len * (i + 1):
                    d = r.read(1024 * 1024)
                    if not d:
                        break
                    w.write(d)
                    start += len(d)
        upload(small_file, dest)
        os.remove(small_file)
        i += 1


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('dest')
    parser.add_argument('file')
    args = parser.parse_args()
    main(**vars(args))
